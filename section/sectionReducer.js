import {handleActions} from 'redux-actions';
import mainSectionReducer from 'app/section/reducers/mainSectionReducer';
import toggleEnableSectionItemReducer from 'app/section/reducers/item/toggleEnableSectionItemReducer';

const initialState = {
	loading: false,
	list: [],
};

export default handleActions({
	...mainSectionReducer,
	...toggleEnableSectionItemReducer,
}, initialState);
