import {createAction} from 'redux-actions';
import * as actions from 'app/section/sectionConstant';
import {
	getEnableSectionItem,
	getLoadingSectionItem,
} from 'app/section/selectors/sectionItemSelector';

const toggleEnableSectionItemRequest = createAction(
	actions.TOGGLE_SECTION_ITEM_ENABLE_REQUEST,
	(id, enable) => ({id, enable}),
);
const toggleEnableSectionItemSuccess = createAction(
	actions.TOGGLE_SECTION_ITEM_ENABLE_SUCCESS,
	id => ({id}),
);
const toggleEnableSectionItemFail = createAction(
	actions.TOGGLE_SECTION_ITEM_ENABLE_FAIL,
	(id, enable) => ({id, enable}),
);

export const toggleEnableSectionItem = async id => (dispatch, getState) => {
	const state = getState();
	const enable = getEnableSectionItem(state, id);
	const loading = getLoadingSectionItem(state, id);

	if (loading) return;

	try {
		dispatch(toggleEnableSectionItemRequest(id, !enable));

		const response = await fetch('/test');

		dispatch(toggleEnableSectionItemSuccess(id));
	} catch (error) {
		console.error(error);

		dispatch(toggleEnableSectionItemFail(id, enable));
	}
};
