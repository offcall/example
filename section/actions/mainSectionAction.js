import {createAction} from 'redux-actions';
import * as actions from 'app/section/sectionConstant';

const getSectionDataRequest = createAction(
	actions.GET_SECTION_DATA_REQUEST,
);
const getSectionDataSuccess = createAction(
	actions.GET_SECTION_DATA_SUCCESS,
	list => ({list}),
);
const getSectionDataFail = createAction(
	actions.GET_SECTION_DATA_FAIL,
);

export const getSectionData = async() => dispatch => {
	try {
		dispatch(getSectionDataRequest());

		const response = await fetch('/test');

		dispatch(getSectionDataSuccess(response));
	} catch (error) {
		console.error(error);

		dispatch(getSectionDataFail());
	}
};
