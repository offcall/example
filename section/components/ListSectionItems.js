import {useSelector} from 'react-redux';
import {getListSection} from 'app/section/selectors/mainSectionSelector';
import {SectionItem} from 'app/section/components/SectionItem';

const ListSectionItemsComponent = () => {
	const list = useSelector(getListSection);

	return (
		<div>
			{list.map(item => (
				<SectionItem key={item.id} id={item.id} />
			))}
		</div>
	);
};

export const ListSectionItems = React.memo(ListSectionItemsComponent);
