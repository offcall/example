import {useSelectorSectionItem} from 'app/section/sectionHook';
import {getTitleSectionItem} from 'app/section/selectors/sectionItemSelector';

const HeaderSectionItemComponent = () => {
	const title = useSelectorSectionItem(getTitleSectionItem);

	return (
		<div>
			{title}
		</div>
	);
};

export const HeaderSectionItem = React.memo(HeaderSectionItemComponent);
