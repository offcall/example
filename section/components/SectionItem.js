import {SectionItemContext} from 'app/section/SectionItem';
import {HeaderSectionItem} from 'app/section/components/HeaderSectionItem';
import {BottomSectionItem} from 'app/section/components/BottomSectionItem';

const SectionItemComponent = ({id}) => (
	<SectionItemContext.Provider value={id}>
		<div className="section-item">
			<HeaderSectionItem />
			<BottomSectionItem />
		</div>
	</SectionItemContext.Provider>
);

export const SectionItem = React.memo(SectionItemComponent);
