import {useDispatch, useSelector} from 'react-redux';
import {toggleEnableSectionItem} from 'app/section/actions/toggleEnableSectionItemAction';
import {useSelectorSectionItem} from 'app/section/sectionHook';
import {
	getEnableSectionItem,
	getIdSectionItem,
} from 'app/section/selectors/sectionItemSelector';

const BottomSectionItemComponent = () => {
	const dispatch = useDispatch();
	const enable = useSelectorSectionItem(getEnableSectionItem);
	const id = useSelectorSectionItem(getIdSectionItem);

	const handleClickToggleEnable = () => {
		dispatch(toggleEnableSectionItem(id));
	};

	return (
		<div>
			enable = ${enable}
			<br />
			<span onClick={handleClickToggleEnable}>toggle enable</span>
		</div>
	);
};

export const BottomSectionItem = React.memo(BottomSectionItemComponent);
