import {getSectionItemById} from 'app/section/selectors/mainSectionSelector';

export const getIdSectionItem = (state, id) => getSectionItemById(state, id).id;
export const getTitleSectionItem = (state, id) => getSectionItemById(state, id).title;
export const getEnableSectionItem = (state, id) => getSectionItemById(state, id).enable;
export const getLoadingSectionItem = (state, id) => getSectionItemById(state, id).loading;
