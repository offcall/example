// Если данных нет, чтобы не приходил каждый раз новый объект
const defaultArray = [];
const defaultObject = {};

export const getStateSection = state => state.sectionReducer;

export const getLoadingSection = state => getListSection(state).loading;
export const getListSection = state => getListSection(state).list || defaultArray;

// Создаем мап по айдишнику {id: {sectionItem}, id2: {sectionItem}}
export const getMapSectionItemById = state => _.keyBy(getListSection(state), 'id');

export const getSectionItemById = (state, id) => getMapSectionItemById(state)[id] || defaultObject;

export const getIsHaveDataSection = state => getListSection(state).length > 0;
