import * as actions from 'app/section/sectionConstant';
import {updateFieldsSectionItem} from 'app/section/sectionHelper';

export default ({
	[actions.TOGGLE_SECTION_ITEM_ENABLE_REQUEST]: (state, {payload: {id, enable}}) => ({
		...state,
		list: updateFieldsSectionItem(state.list, id, () => ({
			loading: true,
			enable,
		})),
	}),
	[actions.TOGGLE_SECTION_ITEM_ENABLE_SUCCESS]: (state, {payload: {id}}) => ({
		...state,
		list: updateFieldsSectionItem(state.list, id, () => ({
			loading: false,
		})),
	}),
	[actions.TOGGLE_SECTION_ITEM_ENABLE_FAIL]: (state, {payload: {id, enable}}) => ({
		...state,
		list: updateFieldsSectionItem(state.list, id, () => ({
			loading: false,
			enable,
		})),
	}),
});
