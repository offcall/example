import * as actions from 'app/section/sectionConstant';

export default ({
	[actions.GET_SECTION_DATA_REQUEST]: state => ({
		...state,
		loading: true,
	}),
	[actions.GET_SECTION_DATA_SUCCESS]: (state, {payload: {list}}) => ({
		...state,
		list,
		loading: false,
	}),
	[actions.GET_SECTION_DATA_FAIL]: state => ({
		...state,
		loading: false,
	}),
});
