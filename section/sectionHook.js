import {SectionItemContext} from 'app/section/SectionItem';
import {useSelector} from 'react-redux';

// Делаем хук, который берет id из контекста и пробрасывает вторым аргументом в селектор
export function useSelectorSectionItem(selector) {
	const id = useContext(SectionItemContext);
	const result = useSelector(state => selector(state, id));

	return result;
};
