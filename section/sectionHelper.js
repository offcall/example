export const updateFieldsSectionItem = (list, id, getNewFields) => list.map(item => String(item.id) === String(id)
	? {
		...item,
		...getNewFields(item),
	}
	: item);
