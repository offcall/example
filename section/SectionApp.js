import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';
import {getIsHaveDataSection} from 'app/section/selectors/mainSectionSelector';
import {getSectionData} from 'app/section/actions/mainSectionAction';
import {ListSectionItems} from 'app/section/components/ListSectionItems';

const SectionAppComponent = () => {
	const dispatch = useDispatch();
	const isHaveData = useSelector(getIsHaveDataSection);

	useEffect(() => {
		dispatch(getSectionData());
	}, []);

	if (!isHaveData) return <div>Loading...</div>;

	return <ListSectionItems />
};

export const SectionApp = React.memo(SectionAppComponent);
